import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  personal:any[]=[
    {
      nombre:'Ignacio',
      edad:32,
      especialidad:"MOTOCICLETAS",
      des:"hombre dedicado a la reparacion y construccion de motocicletas",
    },
    {
      nombre:'Felipe',
      edad:22,
      especialidad:"CAMIONES",
      des:"hombre dedicado a la reparacion de camiones y motores disel",
    },
    {
      nombre:'Roberto',
      edad:24,
      especialidad:"AUTOMOVILES",
      des:"hombre dedicado a la reparacion de automoviles electricos y de gasolina",
    }
  ];
  constructor() { 
    console.log("service online");
    
  }

  enviar_personal(){
    return this.personal;
  }
}
