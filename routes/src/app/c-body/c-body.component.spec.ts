import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CBodyComponent } from './c-body.component';

describe('CBodyComponent', () => {
  let component: CBodyComponent;
  let fixture: ComponentFixture<CBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
