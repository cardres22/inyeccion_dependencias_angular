import { RouterModule, Routes } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { FooterComponent } from './footer/footer.component';
import { CBodyComponent } from './c-body/c-body.component';
import { CContactosComponent } from './c-contactos/c-contactos.component';
import { ContactosComponent } from './contactos/contactos.component';

//servicios

import {PersonalService} from './personal.service';

const routes: Routes = [
  { path: 'contacto', component: ContactosComponent },
  { path: 'info', component: CContactosComponent },
  { path: '', component: CBodyComponent, pathMatch: 'full' },
  { path: '**', redirectTo:'/', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    FooterComponent,
    CBodyComponent,
    CContactosComponent,
    ContactosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    PersonalService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
