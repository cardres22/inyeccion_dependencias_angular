import { Component, OnInit } from '@angular/core';
import { PersonalService } from './../personal.service';
@Component({
  selector: 'app-c-contactos',
  templateUrl: './c-contactos.component.html',
  styleUrls: ['./c-contactos.component.css']
})
export class CContactosComponent implements OnInit {

  personal:any[]=[];
  constructor(private _equipo:PersonalService) {
    this.personal=_equipo.enviar_personal();
   }

  ngOnInit(): void {
  }

  
}
