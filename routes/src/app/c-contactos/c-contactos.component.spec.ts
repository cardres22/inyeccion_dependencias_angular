import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CContactosComponent } from './c-contactos.component';

describe('CContactosComponent', () => {
  let component: CContactosComponent;
  let fixture: ComponentFixture<CContactosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CContactosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CContactosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
