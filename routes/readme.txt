dentro de una carpeta creamos nuestra nueva aplicacion con el comando desde la consola
ng new "nombre de la app"
hacemos un change directory a la nueva carpeta que se genera
cd "nombre de la app"
creamos el servidor que nos va a correr la app
ng serve --open
usualmente corre por el puerto 4200

vamos a crear los diferentes contenedores de manera automatica
con el siguiente comando por consola
ng g c "nombre del contenedor"
para este caso creamos 4 que son [c-body,c-contactos,cabecera,footer]

del archivo app.component.html eliminamos todo el contenido por defecto

en el archivo app.module.ts verificamos que cada uno de los contenedores se haya importado
y en el sector de @ngmodule esten contenidos

en el archivo app.module.ts vamos a colocar todas las rutas, para esto importamos lo siguiente

import { RouterModule, Routes } from '@angular/router';

y luego ya podemos generar una seccion de path como esta
const routes: Routes = [
  { path: 'contacto', component: ContactosComponent },
  { path: 'info', component: CContactosComponent },
  { path: '', component: CBodyComponent, pathMatch: 'full' },
  { path: '**', redirectTo:'/', pathMatch: 'full' }
];

y en la seccion de imports se coloca la siguiente linea
RouterModule.forRoot(routes), de esta manera las rutas ya estan listas

cada uno de los contenedores se generó con un archivo html dentro de el, todo lo que se coloque
ahi como html será lo que se vea reflejado cuando se lo solicite a traves de su etiqueta personal
cada etiqueta se crea con el mismo nombre que se encuentra en el campo selector, dentro del archivo
component.ts de cada contenedor

por ejemplo en la carpeta footer hay un archivo footer.component.ts
ahi se encuentra un campo que se llama selector y tiene por valor "app-footer"
ese será el nombre de la etiqueta con el cual será llamado, pare verlo mejor abra el archivo
app.component.html

para que la pagina no se refresque todo el tiempo cuando uno cambia de seccion en el menu, en lugar de usar href se usa [routerLink]="['/']" donde '/' será la ruta que escojamos

para hacer una inyeccion de dependencias se crea un servicio con el comando
ng g s "nombre del servicio"

se creara un archivo con el siguiente nombre "nombre del archivo".service.ts

dentro del el es importante verificar que este la importacion del core y el modulo @injectable
en este archivo podremos colocar toda la informacion que queramos exportar

en el contenedor donde queremos reflejar esta informacion en su archivo component.ts vamos a importar el servicio y su clase en este ejemplo esta en el contenedor c-contactos, creamos una variable con la misma estructura que vamos a recibir en este caso es un arreglo.
en el constructor instanciamos un nuevo objeto del tipo de dato que nos va a llegar por el servicio y listo, con esa variable ya podemos manipular su informacion.